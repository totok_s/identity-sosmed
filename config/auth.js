// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {

        'facebookAuth' : {
            'clientID'      : '137157066943340', // your App ID
            'clientSecret'  : '5f53968cd36927b854c07c01941acfd1', // your App Secret
            'callbackURL'   : 'https://identity-management.herokuapp.com/auth/facebook/callback',
            'profileFields'   : ['emails','name']
        },

        'twitterAuth' : {
            'consumerKey'       : 'your-consumer-key-here',
            'consumerSecret'    : 'your-client-secret-here',
            'callbackURL'       : 'https://identity-management.herokuapp.com/auth/twitter/callback'
        },

        'googleAuth' : {
            'clientID'      : '342176168041-74qbm8jv8eb34bidlq7sbj3d4359e4pt.apps.googleusercontent.com',
            'clientSecret'  : 'CFOJVWyB9Lw63YdqA2k_oySF',
            'callbackURL'   : 'https://identity-management.herokuapp.com/auth/google/callback'
        }

    };
